package basics;

public class Markermain {
	
	public static void main(String[] args) {
		System.out.println(Marker.category);
		Marker m1=new Marker("Camlin","black");
		Marker m2=new Marker("reynolds","blue");
		 System.out.println(m1.color);
		 System.out.println(m2.color);
		 System.out.println(m1.category);
		 System.out.println(m2.category);
		 m2.color="Green";
		 m2.category="PENS";
		 System.out.println(m1.color);
		 System.out.println(m2.color);
		 System.out.println(m1.category);
		 System.out.println(m2.category);
		 
		 System.out.println(m1.BRAND);
		 
		 
		 System.out.println(m1);				//Fullyqualifed name is going to print which is a one of the methods of OBJECT class.
		 
		 //System.out.println(m1.toString());     
		
		Marker m3 = new Marker("SD","black",34.56);
		 System.out.println(m1);	
		    
		 Marker m4 = m1.clone();
		 System.out.println(m4);
		 m4.setprice(543.345);
		 System.out.println(m4);
	}
	/*public static void main(String[] args) {
		 Marker m1 =new Marker();
		 
		 System.out.println(m1.getprice());
		 System.out.println(m1.brand);
		 System.out.println(m1.color);
		 
		// m1.brand="Reynolds";
		// System.out.println(m1.brand);
		 
		 Marker m2 =new Marker("Reynolds");
		 
		 System.out.println(m2.brand);
		 
		 Marker m3 =new Marker("red","motrolo");
		 System.out.println(m3.color);
		 System.out.println(m3.brand);
		 
	}*/

	/*public static void main(String[] args) 
	
	{
		Marker m1;
	System.out.println(m1); m1 is not initialized we can't print
	
		m1 = new Marker();   //allocates memory where it is a constructor

		System.out.println(m1);
		
		System.out.println(m1.price);// . is to enter the reference 
		
		Marker m2 =new Marker();
		System.out.println(m2.price);
		
		m2.price=-15.5;
		System.out.println(m2.price);
	}
	
	
	
}*/
	/*public static void main(String[] args) {
		Marker m1 =new Marker();
		System.out.println(m1.getprice());
		
		m1.setprice(-111.4);
		System.out.println(m1.getprice());
		
	m1.write("welcome to OOPS concept  ");
		
		m1.write(2342);
		
		m1.write(872354.82753);
		
	}*/
}
