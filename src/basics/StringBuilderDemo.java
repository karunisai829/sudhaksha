package basics;

public class StringBuilderDemo {

	public static void main(String[] args) {
		
	/*StringBuilder s1 = new StringBuilder();
	System.out.println(s1);
	
	s1.append("James gosling");
	System.out.println(s1);
	s1.append(10);										//adding the value to string
	System.out.println(s1);
	s1.insert(13,20);									//adding the value in between string and int
	System.out.println(s1);
	s1.insert(13,' ');									//spacing
	System.out.println(s1);
	//s1.replace(0,18," x");								//replacing the whole first string value to x
	//System.out.println(s1);
	int i = s1.indexOf("0");
	s1.replace(i, i+1, "x");
	System.out.println(s1);*/
	
		StringBuilder s2 = new StringBuilder("2010,2012,2020,2018,2030");
		 
		
		
		for(int i=0;i < s2.length()-1;i++)								//1st method
		{
			i=s2.indexOf("0");
			s2.replace(i, i+1, "x");
			System.out.println(s2);
		}
	
		int i;
		while((i =s2.indexOf("0"))!=-1) {							//2nd method
			s2.replace(1, i++, "x");
		}
		System.out.println(s2);
	
		int count = 0;
		while((i =s2.lastIndexOf("x"))!=-1) {							//3rd method
			count++;
			
			s2.replace(i, i+1, "x");
			if(count==4) {
		
			break;
			}
			}
		System.out.println(s2);
		
	}
}

	
	

