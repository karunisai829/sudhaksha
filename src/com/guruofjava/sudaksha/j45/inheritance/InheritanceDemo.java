package com.guruofjava.sudaksha.j45.inheritance;

public class InheritanceDemo {
	public static void main(String[] args) {
		Animal a1= new Animal();
		Animal a2= new Cat();
		a1.move(21);
		a2.move(22);
		Cat c1 = new Cat();
		c1.move(23);

		Bird b1 = new Bird();
		b1.move(34);


		a1.WhoAmI();
		a2.WhoAmI();
		c1.WhoAmI();



	}


}
